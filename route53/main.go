package main

import (
	"github.com/pulumi/pulumi-aws/sdk/v6/go/aws/route53"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi"
	"github.com/pulumi/pulumi/sdk/v3/go/pulumi/config"
)

func main() {
	pulumi.Run(func(ctx *pulumi.Context) error {
		cfg := config.New(ctx, "")
		domainName := "example.com"
		if param := cfg.Get("domainName"); param != "" {
			domainName = param
		}

		_, err := route53.NewZone(ctx, "primary", &route53.ZoneArgs{
			Name: pulumi.String(domainName),
		})
		if err != nil {
			return err
		}
		return nil
	})
}
